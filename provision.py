#!/usr/bin/env python3

import boto3
import csv
import argparse
from os import path
from datetime import datetime


def create_lorawan_device(client, device_name: str, destination_name: str, dev_eui: str, otaa_v11: dict):

    response = client.create_wireless_device(
        Type='LoRaWAN',
        Name=device_name,
        DestinationName=destination_name,
        LoRaWAN={
            'DevEui': dev_eui,
            'DeviceProfileId': 'string',
            'ServiceProfileId': 'string',
            'OtaaV1_1': otaa_v11,
        },
        Tags=[
            {
                'Key': 'created',
                'Value': str(int(datetime.now().timestamp()))
            },
            {
                'Key': 'device_manufacturer',
                'Value': 'Dragino'
            }
        ]
    )


def get_default_device_destination_name(client, destination_name = 'lorawan_default_destination'):

    response = client.get_parameters(
        Names=[destination_name]
    )

    if response:
        params = response.get('Parameters')
        if params:
            _val = params[0].get('Value')
            if _val:
                return _val
    return False

def register_lorawan_devices(csv_reader):


    ssm_client = boto3.client('ssm')
    dev_destination_name = get_default_device_destination_name(ssm_client)

    if not dev_destination_name:
        print(f'error retrieving the default destination name from AWS SSM, please check... This SSM parameter should exist!')
        exit()

    iot_client = boto3.client('iotwireless')

    current_row = 0
    for row in csv_reader:
        current_row += 1

        # Use heading rows as field names for all other rows.
        if current_row == 1:
            csv_reader.fieldnames = row['undefined-fieldnames']
            continue
        
        dev_name = row.get('device_name').strip()
        dev_eui = row.get('dev_eui').strip()
        dev_app_key = row.get('dev_app_key').strip()
        dev_nwk_key = row.get('dev_nwk_key').strip()
        dev_join_eui_key = row.get('dev_join_eui_key').strip()

        if not dev_name or not dev_app_key or not dev_join_eui_key or not dev_nwk_key:
            print(f'missing value(s) in row: {row}, skipping row...')
            continue

        print(f'creating new dev with {dev_name}, {dev_eui}, {dev_app_key}, {dev_nwk_key}, {dev_join_eui_key}')

        # continue
        new_dev = create_lorawan_device(
                iot_client,
                device_name=dev_name,
                destination_name=dev_destination_name,
                dev_eui=dev_eui,
                otaa_v11={
                    'AppKey': dev_app_key,
                    'NwkKey': dev_nwk_key,
                    'JoinEui': dev_join_eui_key
                }
            )

        print(f'created new device: {new_dev}')



parser = argparse.ArgumentParser()
parser.add_argument('-t', dest="dev_type", help='device type', default="lorawan", choices=['lorawan','catm1'])
parser.add_argument('-f', dest="csv_file", help='import csv file', required=True)

vals = parser.parse_args()
csv_file = vals.csv_file
dev_type = vals.dev_type

if not path.isfile(csv_file):
    print(f"File does not exist: {csv_file}")
    exit()


csv_fp = open(csv_file, 'r')
delimiter = ','
quote_character = '"'
csv_reader = csv.DictReader(csv_fp, fieldnames=[], restkey='undefined-fieldnames', delimiter=delimiter, quotechar=quote_character)

devices_to_create = []

if dev_type == "lorawan":
    register_lorawan_devices(csv_reader)
elif dev_type == "catm1":
    register_catm1_devices(csv_reader)
